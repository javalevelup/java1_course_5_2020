package ru.levelup.lesson7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionExample {

    public static void main(String[] args) {

        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(56);
        integers.add(57);
        integers.add(53);
        integers.add(554);

        Integer fourthElement = integers.get(3);
        System.out.println(fourthElement);

        boolean contains = integers.contains(53);
        System.out.println(contains);

        List<String> strings = new LinkedList<>();
        strings.add("value1");
        strings.add("value2");

        String secondValue = strings.get(1);
        System.out.println(secondValue);

    }

}
