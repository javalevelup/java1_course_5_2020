package ru.levelup.lesson10;

import java.util.function.Predicate;

public class PhoneRamPredicate implements Predicate<Phone> {

    private int ramBound;

    public PhoneRamPredicate(int ramBound) {
        this.ramBound = ramBound;
    }

    @Override
    public boolean test(Phone phone) {
        return phone.getRam() > ramBound;
    }

}
