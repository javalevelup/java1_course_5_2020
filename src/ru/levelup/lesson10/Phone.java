package ru.levelup.lesson10;

public class Phone {

    private String brand;
    private int ram;

    public Phone(String brand, int ram) {
        this.brand = brand;
        this.ram = ram;
    }

    public String getBrand() {
        return brand;
    }

    public int getRam() {
        return ram;
    }

}
