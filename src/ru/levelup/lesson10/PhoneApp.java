package ru.levelup.lesson10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class PhoneApp {

    public static void main(String[] args) {

        Collection<Phone> phones = new ArrayList<>();
        phones.add(new Phone("Oppo", 6));
        phones.add(new Phone("Xiaomi 9", 8));
        phones.add(new Phone("Acer", 4));
        phones.add(new Phone("Samsung", 4));
        phones.add(new Phone("IPhone", 6));
        phones.add(new Phone("Asus", 8));

        // old
        // for (Phone phone : phones) {
        //    System.out.println(phone.getBrand());
        // }

        // new
        // void accept(T t)
        phones.forEach(phone -> System.out.println(phone.getBrand()));


        List<Phone> filteredPhones = phones.stream()
                .filter(phone -> phone.getRam() > 4)
                .collect(Collectors.toList());

        System.out.println();
        filteredPhones.forEach(phone -> System.out.println(phone.getBrand()));

        List<String> phoneBrands = phones.stream() // Stream<Phone>
//                .map(new Function<Phone, String>() {
//                    @Override
//                    public String apply(Phone phone) {
//                        return phone.getBrand();
//                    }
//                })
                .map(phone -> phone.getBrand()) // Stream<String>
                .filter(brand -> brand.length() < 5)
                .collect(Collectors.toList());

        System.out.println();
        phoneBrands.forEach(brand -> System.out.println(brand));

        int countOfSymbols = phones.stream() // Stream<Phone>
                .map(phone -> phone.getBrand()) // Stream<String>
                .mapToInt(brand -> brand.length()) // IntStream
                .sum();

//        countOfSymbols = phones.stream() // Stream<Phone>
//                .mapToInt(phone -> phone.getBrand().length()) // IntStream
//                .sum();

        System.out.println();
        System.out.println("Количество символов в названиях брендов " + countOfSymbols);


        Collection<Collection<Integer>> collection = new ArrayList<>();
        Collection<Integer> bigCollection = collection.stream() // Stream<Collection<Integer>>
                .flatMap(coll -> coll.stream())
                .collect(Collectors.toList());
//                .flatMapToInt(coll -> coll.stream().mapToInt(value -> value)) // Stream<Integer>
//                .sum();









        Collection<Phone> moreThan4RamPhones = new ArrayList<>();
        Predicate<Phone> phonePredicate = new PhoneRamPredicate(4);

        for (Phone phone : phones) {
            if (phonePredicate.test(phone)) {
                // .. put it into another collection
                moreThan4RamPhones.add(phone);
            }
        }
        filterPhone(phones, phonePredicate);

        // ..

        String samsungBrand = "Samsung";
        // new Predicate<Phone> - анонимный внутренний класс
        Predicate<Phone> onlySamsungPredicate = new Predicate<Phone>() {
            @Override
            public boolean test(Phone p) {
                return p.getBrand().contains(samsungBrand);
            }
        };

        // Lambda function
        Predicate<Phone> onlySamsungLambda = ph -> {
            System.out.println("Phone brand: " + ph.getBrand());
            return ph.getBrand().contains(samsungBrand);
        };

        // Arguments
        //  - если нет аргументов у метода
        //      () -> { // code here }
        //  - если один аргумент у метода
        //      (arg) -> { // code here }
        //      arg -> { // code here }
        //  - если у метод 2 и более аргументов
        //      (arg1, arg2) -> { // code here }

        // ->

        // Method body
        //  - если одно выражение в реализации метода
        //      ph -> ph.getBrand().contains(samsungBrand);
        //  - если в реализации строк кода больше чем 1
        //      ph -> { return ph.getBrand().contains(samsungBrand); }

//        if (samsungBrand.equals(phones.iterator().next()))
//            System.out.println(samsungBrand);


//        Supplier<Integer> integerSupplier = () -> {
//            return new Object();
//        };


        filterPhone(phones, onlySamsungPredicate);

        Predicate<Phone> onlyIphonePredicate = new Predicate<Phone>() {
            @Override
            public boolean test(Phone phone) {
                return false;
            }
        };

        // filterPhone(phones, anotherPredicate);

        // for (Phone phone : phones) {
        //            if (phone.getRam() < 4) {
        //                // .. put it into another collection
        //                moreThan4RamPhones.add(phone);
        //            }
        //        }

        // for (Phone phone : phones) {
        //            if (phone.getBrand().length() > 4) {
        //                // .. put it into another collection
        //                moreThan4RamPhones.add(phone);
        //            }
        //        }

        // for (Phone phone : phones) {
        //            if (phone.getBrand().contains("Samsung")) {
        //                // .. put it into another collection
        //                moreThan4RamPhones.add(phone);
        //            }
        //        }


    }

    // filterPhone(phones, phone.getBrand().contains("Samsung"));
    // filterPhone(phones, phone.getRam() > 4);

    // static Collection<Phone> filterPhone(Collection<Phone> phones, boolean condition) {
    static Collection<Phone> filterPhone(Collection<Phone> phones, Predicate<Phone> predicate) {
        Collection<Phone> filteredPhones = new ArrayList<>();
        for (Phone phone : phones) {
            // if (condition) {
            if (predicate.test(phone)) { // <<-- эта часть кода зависит от входных параметров
                filteredPhones.add(phone);
            }
        }
        return filteredPhones;
    }


}
