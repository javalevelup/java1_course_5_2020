package ru.levelup.lesson10;

public class App {

    public static void main(String[] args) {

        OuterClass first = new OuterClass();
        OuterClass.InnerClass firstInner = first.new InnerClass();
        OuterClass.InnerClass second = first.new InnerClass();

        firstInner.setAndPrintIntVariables(4);

        OuterClass.InnerClass secondInner = new OuterClass().new InnerClass();
        secondInner.setAndPrintIntVariables(5);

        OuterClass.NestedClass firstNested = new OuterClass.NestedClass();

        class LocalClass {
            int value;
        }

        LocalClass localClass = new LocalClass();
        localClass.value = 45;

        System.out.println(localClass.value);

    }

}
