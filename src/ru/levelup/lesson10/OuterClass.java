package ru.levelup.lesson10;

public class OuterClass {

    private int intVariable;
    public int publicIntVariable;

    private static int staticIntValue;

    public void printVariables() {
        System.out.println(intVariable + " " + publicIntVariable);
    }

    // Внутренний класс
    // private class InnerClass {
    public class InnerClass {

        public void setIntVariables(int value) {
            intVariable = value;
            publicIntVariable = value;
        }

        public void setAndPrintIntVariables(int value) {
            setIntVariables(value);
            printVariables();
        }

    }

    // Вложенный класс
    public static class NestedClass {

        public void printVariables() {
            System.out.println(staticIntValue);
        }

    }

}
