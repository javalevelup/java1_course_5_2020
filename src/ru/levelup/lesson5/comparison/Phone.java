package ru.levelup.lesson5.comparison;

import java.util.Objects;

@SuppressWarnings("ALL")
public class Phone {

    private String brand;
    private int countOfCameras;
    private int ram;

    public Phone(String brand, int countOfCameras, int ram) {
        this.brand = brand;
        this.countOfCameras = countOfCameras;
        this.ram = ram;
    }

    // x.equals(z) returns true, z.equals(x) -> true
    // x - Phone, z - SubPhone
    // z.equals(x) -> false, x.equals(z) -> true

    // this - phone1
    // object - phone2
    @Override
    public boolean equals(Object obj) {
        // brand - this.brand
        if (this == obj) return true;
        // проверить тип obj, является ли оbj типом Phone
        // obj может быть null
        // 1var
        // instanceof, <object> instanceof <Class>, null instanceof <Class> -> false
        if (!(obj instanceof Phone)) return false;
        // 2var
        if (obj == null || getClass() != obj.getClass()) return false;

        // String brand может быть null

        Phone other = (Phone) obj;
        // (brand != null && brand.equals(other.brand))
        return Objects.equals(brand, other.brand) && // brand.equals(other.brand) &&
                countOfCameras == other.countOfCameras;
    }

    @Override
    public int hashCode() {
//        int result = 17;
//        result = 31 * result + countOfCameras;
//        result = 31 * result + brand.hashCode();
//        return result;
        return Objects.hash(countOfCameras, brand);
    }

}
