package ru.levelup.lesson5.comparison;

@SuppressWarnings("ALL")
public class ComparisonApp {

    public static void main(String[] args) {

        Phone phone1 = new Phone("Xiaomi 9", 4, 6);
        Phone phone2 = new Phone("Xiaomi 9", 4, 6);
        // phone1.equals(null);
        // phone1.equals(phone1);
        boolean isEqual = phone1.equals(phone2);
        System.out.println(isEqual);

        Phone[] phones = new Phone[] { phone1 };
        boolean contains = containsInArray(phones, phone2);
        System.out.println("Contains: " + contains);

    }

    static boolean containsInArray(Object[] objects, Object valueToFind) {
        for (int i = 0; i < objects.length; i++) {
            if (objects[i].hashCode() == valueToFind.hashCode() && objects[i].equals(valueToFind)) {
                return true;
            }
        }
        return false;
    }

}
