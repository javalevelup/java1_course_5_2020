package ru.levelup.lesson3;

// DRY - don't repeat yourself
public class App {

    public static void main(String[] args) {

        int point1X = 4;
        int point1Y = 6; // (4, 6)

        int point2X = 5;
        int point2Y = 8; // (5, 8)

        // 10 points

        int[] xPoints = new int[5]; // массив х координат
        int[] yPoints = new int[5]; // массив y координат

        // p1, point1X, etc... - локальная переменная
        // Переменная непримитивного типа - объект
        // Объект - экземпляр, инстанс, ссылка, instance, reference, object
        Point p1 = new Point(6, 7); // инициализировали переменную класса Point
        // p1.flip();
        // p1 = 123abc42 <- указатель на ячейки памяти
        // p1.x = 4; // 123abc42 -> 123abc45 <- значение х
        p1.setX(4);
        p1.y = 6;

        p1.flip(); // <- вызвали метод из класса Point

        String p1AsString = p1.asString();
        // System.out.println("Point coordinates: x=" + p1.x + ", y=" + p1.y);
        System.out.println(p1AsString);
        // p1AsString = p1AsString + " changed";

        System.out.println(p1.asString());

        p1.asString();

        // p1.changeCoordinates(2, 9);
        System.out.println(p1.asString());

        Point p2 = new Point(8, 9);
        // p2.x = 3;
        p2.y = 8;

        p2.flip();
        // p2.changeCoordinates(5, 7);

        // System.out.println("Point2 coordinates: x=" + p2.x + ", y=" + p2.y);

//      инверсия
//        p1.x = -p1.x;
//        p1.y = -p1.y;


        p2.changePoint(3, 6);
        p2.changePoint(5, 9, "A");
        p2.changePoint("B", 4, 78);

        System.out.println(4);

        Point p3 = new Point();


        // null
        Point p4 = null; // <- p4 = null


    }

}
