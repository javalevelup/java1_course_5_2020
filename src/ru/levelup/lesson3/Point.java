package ru.levelup.lesson3;

// Название метода + его агрументы (важны тип агрументов и их порядок)

// method in class
// 0. void m(double a, int b); -> m(double, int)

// 1. void m(int b, double a); -> m(int, double)
// 2. void m(int a, double b); -> m(int, double)
// 3. void m(double b, int a); -> m(double, int) <- нельзя
// 4. void m(int a, int b); -> m(int, int)
// 5. void m(double a, double b); -> m(double, double)
// 6. double m(double a, int b); -> m(double, int) <- нельзя
// 7. int m(double b, int a); -> m(double, int) <- нельзя


// Access modifiers
//  private - только внутри класса
//  default-package (private-package) - доступ есть внутри класс и внутри пакета
//  protected
//  public - доступно везде
public class Point {

    // данные, которые хранит класс (объект) Point
    private int x; // переменная класса - поле класса (field, class field)
    int y;
    String name;

    public Point() {}

    // Конструктор
    // <Имя класса>() {}
    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

//    Point Point() {
//        return new Point();
//    }

    // Конструктор по-умолчанию
    // public Point() {}

    // инверсия точки -> поменять знак у координат х и y

    // Как пишется метод
    // <тип возращаемого значения> <название метода>(<тип аргумента1> <название аргумента1>, <тип аргумента2> <название аргумент2>, ...)
    public void flip() {
        // тело метода - то, что метод делает
        this.x = -this.x;
        this.y = -this.y;
    }

    // Point coordinates: x=" + p1.x + ", y=" + p1.y
    String asString() {
        String pointAsString = "Point coordinates: x=" + x + ", y=" + y;
        return pointAsString;
    }

    // изменять значения x и y
    private void changeCoordinates(int x, int y) {
        // x = x; // аргумент = аргумент
        // y = y;
        this.x = x;
        this.y = y;
    }

    // Сигнатура метода
    // Название метода + его агрументы (важны тип агрументов и их порядок)
    // changePoint(int,int)
    void changePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Перегрузка метода(-ов) - overloading
    void changePoint(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    void changePoint(String name, int x, int y) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public void setX(int x) {
        this.x = x;
    }

}
