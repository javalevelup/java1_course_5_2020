package ru.levelup.lesson2;

@SuppressWarnings("ALL")
public class CastTypes {

    public static void main(String[] args) {

        int intVariable = 564;
        // ...
        // Неявное расширяющее
        double doubleVariable = intVariable; // преобразование типов int -> double
        // ...
        // Явное сужающее
        long longVariable = (long) doubleVariable; // преобразование типов double -> long

        double realValue = 545.9774;
        int realPart = (int) realValue;
        System.out.println(realPart);

        int a = 130;
        byte b = (byte) a; // [-128, 127]
        System.out.println(b);

        b = (byte)(b - 5); // -126 - 5
        System.out.println(b);

        // 1 - int
        // 1L - long
        // 1D (1d) - double
        // 1F (1f) - float
        b = (byte) (b + 1);

        final byte f = 3;
        final byte c = 4;
        byte d = f + c;

        float f1 = 5353.23f;

//
//        float f1 = 435;
//        float f2 = 445;
//        float res = f1 / f2;


        boolean s = false;
        s &= a < 4; // s = s & (a<4) -> s = s & false -> s = false

    }

}
