package ru.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i = i + 3) {
            System.out.println("Итерация №" + i);
        }

        for (int i = 0; i < 23; i += 6) {
            System.out.println("Итерация №" + i);
        }

        // for (;;) {} - бесконечный цикл
        // for (int j = 0;;) {}
        // for (; j < 5;) {}
        // for (;; j++) {}

        int j = 0;
        while (j < 5) {
            System.out.println("J = " + j);
            j++;
        }


    }

}
