package ru.levelup.lesson2;

public class Array {

    public static void main(String[] args) {

        // arrays (массивы)

//        int price1 = 23;
//        int price2 = 54;
//        int price3 = 543;
//        int price4 = 231;
//        int price5 = 123;
//        int price6 = 453;
//        int average = (price1 * price2 * price3 * price4 * price5 * price6) / 6;

        // массив из 10 элементов типа int
        // int price = 534;
        int[] prices = new int[8]; // << массив значений (элементов), которые имею тип int
        // Обращение к элементу массива
        prices[0] = 23;
        prices[1] = 54;
        prices[2] = 543;
        prices[3] = 231;
        prices[4] = 123;
        prices[5] = 453;
        prices[6] = 987;
        prices[7] = 684;
        // prices[8] => ArrayIndexOutOfBoundsException
        // prices[-1] => ArrayIndexOutOfBoundsException

        System.out.println("Значение первого элемента массива: " + prices[0]);

        // 0, 0d, 0.d, .0d, 0.0d
        // double average = 0.d;

        int sum = 0;
        // 1. index = 0, 0 < 8 .... index => 1
        // 2. index = 1, 1 < 8 .... index => 2
        // 3. index = 2, 2 < 8 .... index => 3
        // 4. index = 3, 3 < 8 .... index => 4
        // 5. index = 4, 4 < 8 .... index => 5
        // 6. index = 5, 5 < 8 .... index => 6
        // 7. index = 6, 6 < 8 .... index => 7
        // 8. index = 7, 7 < 8 .... index => 8
        // 9. index = 8, 8 < 8 -> НЕВЕРНО
        for (int index = 0; index < prices.length; index++) {
            System.out.println("prices[" + index + "]=" + prices[index]);
            sum += prices[index]; // sum = sum + prices[index];
        }

        double average = (double) sum / prices.length;
        System.out.println("Средняя цена: " + average);

        //
        String[] strings = new String[prices.length];
        for (int i = 0; i < prices.length; i++) {
            strings[i] = prices[i] + "";
        }


    }

}
