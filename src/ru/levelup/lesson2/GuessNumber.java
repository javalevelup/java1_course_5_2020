package ru.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        // Считывать данные с консоли
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите число:");
        // В переменную enteredNumber будет записано число, которое мы ввели с клавиатуры
        int enteredNumber = sc.nextInt();
        System.out.println("Вы ввели число: " + enteredNumber);

        // Генерация псевдослучайных чисел
        Random r = new Random();
        int secretNumber = r.nextInt(5); // [0..5) = [0..4]

//        if (enteredNumber == secretNumber) {
//            System.out.println("Вы угадали!");
//        } else {
//            // System.out.println("Вы не угадали число " + secretNumber);
//            if (enteredNumber > secretNumber) {
//                System.out.println("Вы ввели число, которое больше, чем загаданное");
//            } else {
//                System.out.println("Вы ввели число, которое меньше, чем загаданное");
//                // return;
//            }
//        }


        // if..else if...
        if (enteredNumber == secretNumber) {
            System.out.println("Вы угадали!");
        } else if (enteredNumber > secretNumber) {
            System.out.println("Вы ввели число, которое больше, чем загаданное");
        } else {
            System.out.println("Вы ввели число, которое меньше, чем загаданное");
        }

        // Область видимости переменной
//        int a = 23;
//        if (enteredNumber < secretNumber) {
//            System.out.println(a);
//            int b = 43;
//        }
//        System.out.println(b);
    }

}