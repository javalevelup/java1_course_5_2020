package ru.levelup.lesson1;

// Full class name: ru.levelup.lesson1.PrimitiveTypes
public class PrimitiveTypes {

    public static void main(String[] args) {
        // 1101 (2СС) = 1 * 2^0 + 0 * 2^1 + 1 * 2^2 + 1 * 2^3 = 1 + 4 + 8 = 13 (10CC)
        // 01111111 (2CC) = 127 (10CC)
        // 11111110 (2CC) = -1 (10СС)

        int a; // << объявление переменной a
        // some code...
        a = 65; // << записали новое значение в переменную а
        System.out.println(a); // << вывести значение переменной а в консоль (на экран)

        int b = 234; // << инициализация переменной b

        int c = a + b;
        System.out.println("Сумма = " + c); // Строка + число -> Строка + строка = СтрокаСтрока

        double d = 54.2;
        double d2 = 534.3;

        double multiply = d * d2;
        System.out.println(multiply);

        int first = 18;
        int second = 5;
        int mod = first % second;
        System.out.println(mod);

        System.out.println();
        // a = 65, b = 234, a + b = 299
        System.out.println(a + b + " = a + b"); // 299 = a + b
        // sout + Tab (Enter)
        System.out.println("a + b = " + a + b); // a + b = 65234 -> S + int + int -> S + int -> S

        // increment, decrement
        // ++, --
        // a++ -> a = a + 1
        // a-- -> a = a - 1
        a++; // 66
        System.out.println(a);
        a = a + 1; // 67
        System.out.println(a);

        // префиксный инкремент (декремент) - ++a -> сначала увеличь значение а, потом выполни другие операции, если они есть
        // постфиксный инкремент (декремент) - a++ -> сначала выполни другие операции, если они есть, потом увеличь значение а
        // ++a;
        // a++;
        System.out.println();
        // 67
        System.out.println(a++); // 67
        System.out.println(++a); // 69

        // 67
        // System.out.println(a);
        // a = a + 1;
        // a = a + 1;
        // System.out.println(a);

    }

}
